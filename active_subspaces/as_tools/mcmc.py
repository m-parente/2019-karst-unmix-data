import itertools as it
import numpy as np
import numpy.fft as fft
import numpy.random as rnd


def mh_mcmc(dens, proposal_sampler, x1, steps, n_log=50):
    """
    Runs standard Metropolis-Hastings algorithm for MCMC.

    :param dens: Function representing the density to generate samples from
    :param proposal_sampler: Function producing MCMC proposals
    :param x1: Starting point for MCMC
    :param integer steps: Number of MCMC steps
    :param n_log: Distance between two outputs of the acceptance rate
    """
    samples = np.empty((steps, len(x1)))
    xk = x1
    samples[0, :] = xk
    dens_xk = dens(xk)
    k = 1
    accptd = 0

    while k < steps:
        x_ = proposal_sampler(xk)
        dens_x_ = dens(x_)

        accpt_ratio = np.min([1, dens_x_ / dens_xk]) \
            if dens_xk > 0 else \
            (1 if dens_x_ > 0 else 0)

        if accpt_ratio >= rnd.uniform():
            xk = x_
            dens_xk = dens_x_
            accptd += 1

        samples[k, :] = xk

        if (k + 1) % n_log == 1:
            print "State %i: %s" % (k, xk)
            print "Acceptence rate at step %i: %s" % (
                k, "{0:.3}".format(accptd / float(k) * 100))

        k += 1

    return samples


def _autocorr(samples):
    N = len(samples)
    m = np.mean(samples)

    nfft = 2**int(np.ceil(np.log2(N)))
    freq = fft.fft(samples - m, n=nfft)
    acf = np.real(fft.ifft(freq * np.conj(freq))[:N]) / 4 * nfft

    return acf / acf.flat[0]


def _auto_corr_time(samples, maxlag):
    acf = _autocorr(samples)

    if maxlag is None:
        acf = np.array(list(it.takewhile(lambda x: x > 0., acf)))
        maxlag = len(acf)
    # print "Maxlag: ", len(acf)

    return 1 + 2 * np.sum(acf), acf, maxlag


def stats(samples, burn_in, maxlag=None):
    """
    Computes statistics for MCMC samples.

    The statistics consist of the minimum effective sample size (ESS), the ESSs, the autocorrelation times and the autocorrelations for every component

    :param samples: List of MCMC samples
    :param integer burn_in: Number of samples regarded as burn-in
    :param integer maxlag: Number of autocorrelations taken into account for computing ESSs
    """
    samples = samples[burn_in:]
    N, dims = np.shape(samples)
    ess = np.empty(dims)
    auto_corr_times = np.empty(dims)
    acfs = [None] * dims
    maxlags = np.empty(dims)

    for i in range(dims):
        auto_corr_time, acf, maxlags[i] = _auto_corr_time(samples[:, i], maxlag)
        ess[i] = N / auto_corr_time
        auto_corr_times[i] = auto_corr_time
        acfs[i] = acf

    if maxlag is None:
        # Cut every acf on the minimal size of all acf's
        # l = np.min(map(len, acfs))
        maxlag = int(np.min(maxlags))

    acfs = map(lambda acf: acf[:maxlag], acfs)

    return np.min(ess), ess, auto_corr_times, np.array(acfs)


def pick_eff_samples(samples, burn_in, maxlag=None):
    """
    Picks effective samples out of a set of sample according to the effective sample size (ESS).

    :param samples: List of MCMC samples
    :param integer burn_in: Number of samples regarded as burn-in
    :param integer maxlag: Number of autocorrelations taken into account for computing ESSs
    """
    min_ess = stats(samples, burn_in, maxlag)[0]
    return samples[burn_in::(len(samples)-burn_in) / int(min_ess)]
