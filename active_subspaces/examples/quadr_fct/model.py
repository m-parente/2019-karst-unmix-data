from as_tools.gradients import cfd
from misc import Settings

import numpy as np

settings = Settings('settings')
A = np.loadtxt('%s/A.txt' % settings.dir_tmp)


class Model:
    def __init__(self, id, x):
        self.id = id
        self.x = x

    def qoi(self):
        return 0.5 * np.dot(self.x, np.dot(A, self.x))

    def jac(self):
        # return np.array([np.dot(A, self.x)])

        def f(x):
            pb = Model('%d_%d' % (self.id, hash(str(x))), x)
            return pb.qoi()

        self.jac = cfd.approximate_gradient_at(self.x, f, h=1e-3)

        return self.jac


class AbstractModel:
    def instantiate(self, x, id=-1):
        return Model(id, x)
