import env

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()

dir_mcmc = env.dir_mcmc
k = env.k


autocorrs = np.loadtxt('%s/y_autocorrs_%id.txt' % (dir_mcmc, k))

plt.figure()

for i in range(len(autocorrs)):
    autocorr = autocorrs[i]
    plt.plot(autocorr, 'o', label='$y_%i$' % i)

plt.xlabel('Lag')
plt.ylabel('Correlation')
plt.legend()
plt.show()
