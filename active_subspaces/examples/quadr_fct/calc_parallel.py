import env

import os

from as_tools import utils


dir = env.dir_as_G_jacG
os.system('mkdir -p %s' % dir)

N = 250
samples = env.prior_sample(N)
script_name = 'calc_template.py'

utils.construct_G_jacG_parallel(samples, script_name, dir)
