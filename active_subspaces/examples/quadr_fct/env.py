import sys
sys.path.append('../../')

import model

from misc import Settings
from as_tools import bayinv as bi
from as_tools import utils

from glob import glob
import natsort
import numpy as np
import numpy.linalg as la
import numpy.random as rnd
import os
import scipy.stats as stat


settings = Settings('settings')

dir_as = settings.dir_as
dir_as_G_jacG = settings.dir_as_G_jacG
dir_mcmc = settings.dir_mcmc
dir_tmp = settings.dir_tmp
os.system('mkdir -p %s %s %s %s' % (dir_as, dir_as_G_jacG, dir_mcmc, dir_tmp))

sigma = settings.noise_level
x_true = np.array(settings.x_true)

n = settings.n
k = settings.k

W = np.loadtxt('%s/asm_eigVecs.txt' % dir_as)
L = np.loadtxt('%s/asm_eigVals.txt' % dir_as)
W1 = W[:, :k]
W2 = W[:, k:]

prior = stat.multivariate_normal(mean=np.zeros(n)).pdf
prior_y = stat.multivariate_normal(mean=np.zeros(k)).pdf


def prior_sample(size=1):
    return rnd.multivariate_normal(mean=np.zeros(n), cov=np.eye(n), size=size)


def prior_y_sample(size=1):
    return rnd.multivariate_normal(mean=np.zeros(k), cov=np.eye(k), size=size)


def prior_z_y_sample(y=None, size=10):
    return rnd.multivariate_normal(mean=np.zeros(n-k), cov=np.eye(n-k), size=size)


xs = np.loadtxt('%s/samples.txt' % dir_as_G_jacG)

GFiles = natsort.natsorted(glob('%s/G*.txt' % dir_as_G_jacG))
jacGFiles = natsort.natsorted(glob('%s/jacG*.txt' % dir_as_G_jacG))
Gs = np.array(map(lambda GFile: [np.loadtxt(GFile)], GFiles))
jacGs = np.array(map(lambda jacGFile: [np.loadtxt(jacGFile)], jacGFiles))

noise_level = settings.noise_level
data = np.array([np.loadtxt(settings.data_file)])

_variance = (data * noise_level)**2
_invNoiseCovMat = np.diag(1 / _variance)

bay_inv_pb = bi.BayesianInverseProblem(
    data, model.AbstractModel(), _invNoiseCovMat)
misfit = bay_inv_pb.misfit
misfitG = bay_inv_pb.misfitG
