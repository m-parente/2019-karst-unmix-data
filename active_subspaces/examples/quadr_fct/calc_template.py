import env

import numpy as np
import sys


dir = env.dir_as_G_jacG


def run(id, sample):
    print "Sample %i" % id

    m = env.bay_inv_pb.abs_model.instantiate(sample)
    qoi = m.qoi()
    jac = m.jac()

    np.savetxt('%s/G%d.txt' % (dir, id), [qoi])
    np.savetxt('%s/jacG%d.txt' % (dir, id), jac)


def usage():
    print "usage: id parameters"


if __name__ == "__main__":
    if len(sys.argv) == env.n + 2:
        run(int(sys.argv[1]), np.array(sys.argv[2:], dtype=float))
    else:
        usage()
