import sys
sys.path.append("../../")

import env
import plotters
from as_tools import asm
from as_tools import utils

import numpy as np


settings = env.settings

n_boot = settings.n_bootstrap_iters

eig_vals, eig_vecs, min_eig_vals, max_eig_vals, min_subspace_errors, max_subspace_errors, mean_subspace_errors = \
    asm.compute_active_subspace_from_samples(
        env.Gs, env.jacGs, env.bay_inv_pb, n_boot)

utils.save_active_subspace(eig_vals, eig_vecs, min_eig_vals, max_eig_vals,
                           min_subspace_errors, max_subspace_errors, mean_subspace_errors,
                           dir=env.dir_as)

n_plot_eig_vals = 10
plotters.plot_active_subspace(eig_vals, eig_vecs, n_plot_eig_vals, min_eig_vals, max_eig_vals,
                              min_subspace_errors, max_subspace_errors, mean_subspace_errors, )
