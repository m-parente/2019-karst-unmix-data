import env

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()


k = env.k
dir_mcmc = env.dir_mcmc

y_samples_prior = env.prior_sample(size=10000)
eff_y_samples = np.loadtxt('%s/eff_y_samples_%id.txt' % (dir_mcmc, k))

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)

sns.distplot(y_samples_prior[:, 0], kde=False, hist=True,
             norm_hist=True, bins=20, ax=ax1, axlabel='$w_1^Tx$')
sns.distplot(y_samples_prior[:, 1], kde=False, hist=True,
             norm_hist=True, bins=20, ax=ax2, axlabel='$w_2^Tx$')
sns.distplot(eff_y_samples[:, 0], kde=False, hist=True, norm_hist=True, bins=20, ax=ax3)
sns.distplot(eff_y_samples[:, 1], kde=False, hist=True, norm_hist=True, bins=20, ax=ax4)

ax1.set_ylabel('Prior')
ax3.set_ylabel('Posterior')

plt.tight_layout()
plt.subplots_adjust(hspace=0.4, wspace=0.4)

plt.show()
