import env
from as_tools import asm
from as_tools import mcmc

import numpy as np
import numpy.random as rnd
import sys
import time as tm


settings = env.settings

dir_mcmc = env.dir_mcmc

k = env.k
W1, W2 = env.W1, env.W2


if len(sys.argv) < 2:
    # Construct the response surface -----------------
    xs = env.xs
    misfits = [env.misfitG(G) for G in env.Gs]

    resp_surface = asm.response_surface(xs, misfits, W1, poly_order=4)
    # ------------------------------------------------

    prior_y = asm.marginal_prior_y(env.prior_sample(size=10000),
                                   W1, kde_bandwidth=0.12, kde_kernel='gaussian')

    start = tm.time()

    y_samples = asm.as_mcmc_with_response_surface(
        resp_surface,
        W1, W2,
        proposal_sampler=lambda yk: rnd.normal(loc=yk, scale=0.15),
        prior_y=prior_y,
        y1=np.zeros(k),
        steps=1 * 10**5,
        n_log=5000)

    durat = tm.time() - start

    burn_in = settings.burn_in_act_var
    min_ess, ess, corr_times, acfs = mcmc.stats(
        y_samples, burn_in=burn_in, maxlag=None)
    print "MCMC stats (min ess, ess, corr times): %f,%s,%s" % (
        min_ess, ess, corr_times)

    eff_y_samples = mcmc.pick_eff_samples(y_samples, burn_in, maxlag=None)
    print "# Eff. samples: %i" % len(eff_y_samples)
    print "Elapsed seconds: %f" % durat
    print "# Eff. samples / sec: %f" % (len(eff_y_samples) / durat)

    np.savetxt('%s/y_samples_%id.txt' % (dir_mcmc, k), y_samples)
    np.savetxt('%s/eff_y_samples_%id.txt' % (dir_mcmc, k), eff_y_samples)
    np.savetxt('%s/y_autocorrs_%id.txt' % (dir_mcmc, k), acfs)
else:
    if sys.argv[1] == 'i':
        eff_y_samples = np.loadtxt('%s/eff_y_samples_%id.txt' % (dir_mcmc, k))
    else:
        print "Unknown argument '%s'" % sys.argv[1]
        exit()

    m = 100
    eff_y_samples = eff_y_samples[rnd.choice(len(eff_y_samples), m)]

    start = tm.time()
    x_samples = asm.mcmc_active_to_original_samples(eff_y_samples,
                                                    W1, W2,
                                                    prior=env.prior,
                                                    proposal_sampler=lambda zk: rnd.normal(
                                                        loc=zk, scale=0.65),
                                                    z1=np.zeros(env.n-k),
                                                    steps_per_active_sample=1 * 10**4,
                                                    burn_in=settings.burn_in_inact_var,
                                                    n_log=2000)
    x_samples = np.concatenate(x_samples)
    print "%f sec elapsed." % (tm.time() - start)
    print '# of x samples: %i' % len(x_samples)

    np.savetxt('%s/x_samples_%id.txt' % (dir_mcmc, k), x_samples)
