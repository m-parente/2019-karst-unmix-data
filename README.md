This repository contains code and data for reproducibility of results from _Bayesian calibration and sensitivity analysis for a karst aquifer model using active subspaces_ by Teixeira Parente et al., 2019.

For using _calc_parallel.py_, the launcher script ([https://github.com/TACC/launcher](https://github.com/TACC/launcher)) is required.
